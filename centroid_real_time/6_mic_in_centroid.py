# -*- coding: utf-8 -*-
"""
Created on Tue May  4 14:48:15 2021

@author: user
"""

# installing pyaudio in windows:
# https://stackoverflow.com/questions/52283840/i-cant-install-pyaudio-on-windows-how-to-solve-error-microsoft-visual-c-14
# for other python distributions (e.g. 3.8)
# https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio
# 
# for mac m1 - apple silicon
# https://stackoverflow.com/questions/68251169/unable-to-install-pyaudio-on-m1-mac-portaudio-already-installed
# install homebrew
# https://mac.install.guide/homebrew/index.html

import pyaudio
# import wave
import numpy as np
import matplotlib.pyplot as plt
from time import sleep
from threading import Thread
import librosa

p = pyaudio.PyAudio()
# show devices
for i in range(p.get_device_count()):
    d = p.get_device_info_by_index(i)
    print(d)

# select device for input and output
mic_device_index = 3

WINDOW_SIZE = 2048
CHANNELS = 1
RATE = 44100

FFT_FRAMES_IN_SPEC = 20

# global
global_blocks = np.zeros( ( FFT_FRAMES_IN_SPEC, WINDOW_SIZE ) )
fft_frame = np.array( WINDOW_SIZE//2 )
win = np.hamming(WINDOW_SIZE)
spec_img = np.zeros( ( WINDOW_SIZE//2 , FFT_FRAMES_IN_SPEC ) )


user_terminated = False

#------------------------------------------------------------------------------------

def callback( in_data, frame_count, time_info, status):
    global global_blocks, fft_frame, win, spec_img
    # we don't have a file - just read in_data from mic
    # data_in is in the same format as the data retrieved from file,
    # in case the file is 16-bit
    # block_bytes_from_file = f.readframes(WINDOW_SIZE)
    numpy_block_from_bytes = np.frombuffer( in_data , dtype='int16' )
    # begin with a zero buffer
    # begin with a zero buffer
    block_for_speakers = np.zeros( (numpy_block_from_bytes.size , CHANNELS) , dtype='int16' )
    # pitch shift
    # block_for_speakers[:,0] = np.r_[ numpy_block_from_bytes[::2] , numpy_block_from_bytes[::2] ]
    # 0 is left, 1 is right speaker / channel
    block_for_speakers[:,0] = numpy_block_from_bytes
    # for plotting
    # audio_data = np.fromstring(in_data, dtype=np.float32)
    if len(win) == len(numpy_block_from_bytes):
        frame_fft = np.fft.fft( win*numpy_block_from_bytes )
        p = np.abs( frame_fft )*2/np.sum(win)
        # translate in dB
        fft_frame = 20*np.log10( p[ :WINDOW_SIZE//2 ] / 32678 )
        spec_img = np.roll( spec_img , -1 , axis=1 )
        spec_img[:,-1] = fft_frame[::-1]
        global_blocks = np.roll( global_blocks, -1, axis=0 )
        global_blocks[-1,:] = numpy_block_from_bytes
    return (block_for_speakers, pyaudio.paContinue)

def user_input_function():
    k = input('press "s" to terminate (then press "Enter"): ')
    print('pressed: ', k)
    if k == 's' or k == 'S':
        global user_terminated
        user_terminated = True
        print('user_terminated 1: ', user_terminated )

# %% create output stream
output = p.open(format=pyaudio.paInt16,
                channels=CHANNELS,
                rate=RATE,
                output=True,
                input=True,
                input_device_index=mic_device_index,
                frames_per_buffer=WINDOW_SIZE,
                stream_callback=callback,
                start=False)

output.start_stream()

threaded_input = Thread( target=user_input_function )
threaded_input.start()

# after starting, check when n empties (file ends) and stop
while output.is_active() and not user_terminated:
    plt.clf()
    plt.subplot(2,1,1)
    plt.imshow( spec_img[ WINDOW_SIZE//4: , : ] , aspect='auto' )
    # plt.axis([0,WINDOW_SIZE//8, -120,0])
    s = np.reshape(global_blocks, WINDOW_SIZE*FFT_FRAMES_IN_SPEC)
    plt.subplot(2,1,2)
    plt.plot(s)
    plt.axis([0, s.size,-np.iinfo('int16').max, np.iinfo('int16').max])
    c = librosa.feature.spectral_centroid( y=s, sr=RATE, n_fft=WINDOW_SIZE, hop_length=WINDOW_SIZE)
    # c[0] includes the centroid values, you need to plot then on the
    # spectrogram subplot
    plt.show()
    plt.pause(0.01)

print('stopping audio')
output.stop_stream()
